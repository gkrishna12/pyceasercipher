import string

def load_words(file_name):
    print('Loading word list from file...')
    in_file = open(file_name, 'r')
    line = in_file.readline()
    word_list = line.split()
    print('  ', len(word_list), 'words loaded.')
    in_file.close()
    return word_list

def is_word(word_list, word):
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

def get_story_string():
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

WORDLIST_FILENAME = 'words.txt'
wordlist = load_words(WORDLIST_FILENAME)

def decrypt_story():
    cipher = CiphertextMessage(get_story_string)
    return cipher.decrypt_message()

class Message(object):
    def __init__(self, text):
        self.message_text = text
        self.valid_words = wordlist

    def get_message_text(self):
        return self.message_text

    def get_valid_words(self):
        return self.valid_words[:]
        
    def build_shift_dict(self, shift):
        lowerCase = string.ascii_lowercase
        upperCase = string.ascii_uppercase
        
        index = 0
        shiftDict = {}
        
        for letter in lowerCase :
            shiftDict[lowerCase[index]] = lowerCase[(index + shift) % 26 ]
            shiftDict[upperCase[index]] = upperCase[(index + shift) % 26 ]
            index += 1
        
        return shiftDict

    def apply_shift(self, shift):
        cryptedText = ''
        shiftDict = self.build_shift_dict(shift)
        for letter in self.message_text :
            if letter in shiftDict :
                cryptedText += shiftDict[letter]
            else :
                cryptedText += letter
        return cryptedText
        

class PlaintextMessage(Message):
    def __init__(self, text, shift):
        Message.__init__(self,text)
        self.shift = shift
        self.encrypting_dict = self.build_shift_dict(self.shift)
        self.message_text_encrypted = self.apply_shift(self.shift)

    def get_shift(self):
        return self.shift

    def get_encrypting_dict(self):
        return self.encrypting_dict.copy()

    def get_message_text_encrypted(self):
        return self.message_text_encrypted

    def change_shift(self, shift):
        self.shift = shift
        self.encrypting_dict = self.build_shift_dict(self.shift)
        self.message_text_encrypted = self.apply_shift(self.shift)


class CiphertextMessage(Message):
    def __init__(self, text):
        Message.__init__(self,text)

    def decrypt_message(self):
        currentShift = 0
        bestShift= 0
        currentWordCount = 0
        bestWordCount = 0
        bestWords = ''
        words = self.message_text.split()
        
        while currentShift <= 25 :
            currentWordCount = 0
            for word in words :
                if Message.apply_shift(Message(word),currentShift) in self.get_valid_words() :
                    currentWordCount += 1
            if currentWordCount > bestWordCount :
                bestWordCount = currentWordCount
                bestShift = currentShift
            currentShift += 1
        
        return (bestShift, self.apply_shift(bestShift))
  

ciphertext = CiphertextMessage(get_story_string())
print('Actual Output:', ciphertext.decrypt_message())

